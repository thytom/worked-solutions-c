#include <stdio.h>

// Vertical is a bit of a pain, I wouldn't bother.
// Yes, I could use a do/while loop here and it would work better.
// I just didn't want to over-complicate the program.

/*
NOTE:
	For these programs, they read from STDIN. They will appear not to
	work if you just run them.

	To test this program, you need to pipe it some input:

	echo "Test input" | ./program
   */

int main()
{
	char c;
	char last_char;

	int word_length = 0;
	while(c = getchar())
	{
		if(c == ' ' || c == EOF)
		{
			if(last_char == ' ') continue;//Ignore repeated spaces
			for(int i = 0; i < word_length; i++)
				putchar('#');
			putchar('\n');
			word_length = 0;

			if(c == EOF)
				break;
		}
		else
		{
			word_length++;
		}
		last_char = c;
	}
}
