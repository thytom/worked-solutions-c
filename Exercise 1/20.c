#include <stdio.h>
#define TABSTOB 4

int main()
{
	char c;
	while((c = getchar()) != EOF)
	{
		if(c == '\t')
			for(int i = 0; i < TABSTOB; i++)
				putchar(' ');
		else
			putchar(c);
	}
	return 0;
}
