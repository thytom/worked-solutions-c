#include <stdio.h>
#define TABSTOP 4

int main()
{
	char c;
	int blank_counter;

	while((c = getchar()) != EOF)
	{
		if(c != ' ')
		{
			putchar(c);
			continue;
		}
		while(c == ' ')
		{
			blank_counter++;
			c = getchar();
		}
		while(blank_counter > 0)
		{
			if(blank_counter-- >= TABSTOP)
			{
				putchar('\t');
				blank_counter -= 3;
			}
			else
				putchar(' ');
		}
		putchar(c);
	}
}
