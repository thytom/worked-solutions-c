#include <stdio.h>
#include <ctype.h>

int main()
{
	int alphabet_frequencies[26]; /* 26 letters in the alphabet. */
	char c;

	for(int i = 0; i < 26; i++)
		alphabet_frequencies[i] = 0;

	while((c = getchar()) != EOF)
	{
		alphabet_frequencies[tolower(c) - 'a']++;
	}
	for(int i = 0; i < 26; i++)
	{
		/* printf("%c : %6d\n", i + 'a', alphabet_frequencies[i]); */
		for(int j = 0; j < alphabet_frequencies[i]; j++)
			putchar('#');
		putchar('\n');
	}
}
