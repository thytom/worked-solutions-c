#include <stdio.h>

/*
NOTE:
	For these programs, they read from STDIN. They will appear not to
	work if you just run them.

	To test this program, you need to pipe it some input:

	echo "Test input" | ./program
   */

int main(){
	char c;
	int blanks, tabs, newlines;
	blanks = tabs = newlines = 0;

	while((c = getchar()) != EOF)
	{
		switch(c)
		{
			case '\n' : newlines++; break;
			case '\t' : tabs++; break;
			case ' '  :  blanks++; break;
			default : break;
		}

		if(c == '\n')
			newlines++;
		else if(c == '\t')
			tabs++;
		else if(c == ' ')
			blanks++;
	}

	printf("Blanks: %d\nTabs: %d\nNewlines: %d\n", blanks, tabs, newlines);
}

/* For the while loop, you could also use:
	while((c = getchar()) != EOF)
	{
		switch(c)
		{
			case '\n' : newlines++; break;
			case '\t' : tabs++; break;
			case ' '  :  blanks++; break;
			default : break;
		}
	}
	*/
