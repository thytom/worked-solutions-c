#include <stdio.h>
#define MAXLINE 1000

int get_line(char line[], int lim);
void removetrailing(char line[], int length);

int main()
{
	int line_length;
	char line[MAXLINE];

	while((line_length = get_line(line, MAXLINE)) != -1)
	{
		if(line_length == 1)
			continue;
		removetrailing(line, line_length);
		printf("%s", line);
	}
	return 0;
}

int get_line(char line[], int lim)
{
	char c;
	int i;
	for(i = 0; (i < lim) && ((c = getchar()) != EOF) && (c != '\n'); i++)
	{
		line[i] = c;
	}
	if(c == EOF && i == 0)
		return -1;
	line[i++] = '\n';
	line[i]   = '\0';
	return i;
}

void removetrailing(char line[], int length)
{
	int i = length;
	while((line[i] == '\t') || (line[i] == ' '))
		i--;
	line[i + 1] = '\n';
	line[i + 2] = '\0';
}
