#include <stdio.h>

void for_loop_solution();

main()
{
	// Uses a while loop, like in the book
	float fahr, celsius;

	int start, end, increment;

	start		= 0;
	end 		= 100;
	increment 	= 5;

	celsius = start;
	while(celsius <= end)
	{
		fahr = (celsius / (5.0/9.0)) + 32.0;
		printf("%3.0f %6.1f\n", celsius, fahr);
		celsius += increment;
	}
}

// Uses a for loop, like a sensible person :P
void for_loop_solution()
{
	float fahr, celsius;

	int start, end, increment;

	start		= 0;
	end 		= 100;
	increment 	= 5;

	for(celsius = start; celsius <= end; celsius += increment)
	{
		fahr = (celsius / (5.0/9.0)) + 32.0;
		printf("%3.0f %6.1f\n", celsius, fahr);
	}
}
