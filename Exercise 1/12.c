#include <stdio.h>

/*
NOTE:
	For these programs, they read from STDIN. They will appear not to
	work if you just run them.

	To test this program, you need to pipe it some input:

	echo "Test input" | ./program
   */

int main(){
	char c;

	while((c = getchar()) != EOF)
	{
		if(c == ' ')
			putchar('\n');
		else
			putchar(c);
	}
}
