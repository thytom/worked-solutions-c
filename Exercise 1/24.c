#include <stdio.h>

/*
   This checks the bracket types, but isn't great....
   Needs improving.
   */

/* Every even place is an opener, odd a closer */
const char braces[6] = {
	'(',
	')',
	'{',
	'}',
	'[',
	']',
};


char c;
int char_counter = 0;
int	line_counter = 0;

int enter_pair(char expect, int line_number, int character_number);
int is_in(const char s[], char c);

int main(){
	while((c = getchar()) != EOF)
	{
		int x;
		if((x = is_in(braces, c)) >= 0)
		{
			if(x % 2 == 0)
			{
				/* printf("%d: ", x); */
				x = enter_pair(braces[x + 1], line_counter, char_counter);
				if (x == 0)
					return 1;
			}else{
				printf("%c Is out of place.", c);
			}
		}
		char_counter++;
		if(c == '\n')
		{
			char_counter = 0;
			line_counter++;
		}
	}
	return 0;
}

int enter_pair(char expect, int line_number, int character_number)
{
	/* printf("Entered pair with %c, expecting %c\n", c, expect); */
	while((c = getchar()) != EOF)
	{
		if(c == expect)
		{
			/* printf("Found matching %c. Exiting.\n", c); */
			return 1;
		}
		int x;
		if((x = is_in(braces, c)) >= 0)
		{
			if(x % 2 == 0)
			{
				/* printf("%d: ", x); */
				x = enter_pair(braces[x + 1], line_counter, char_counter);
				if (x == 0)
					return 1;
			}else{
			}
		}
		char_counter++;
		if(c == '\n')
		{
			char_counter = 0;
			line_counter++;
		}
	}
	printf("Error.", line_number, character_number, expect);
	return 0;
}

int is_in(const char s[], char c)
{
	for(int i = 0; i < 6 && s[i] != '\0'; i++)
		if(s[i] == c)
		{
			/* printf("%c and %c match.\n", c, s[i]); */
			return i;
		}
	return -1;
}
