#include <stdio.h>

void for_loop_solution();

main()
{
	// Uses a while loop, like in the book
	float fahr, celsius;

	int start, end, increment;

	start		= 300;
	end 		= 0;
	increment 	= -5;

	celsius = start;
	while(celsius >= end) // This is >= now and not <= like in 1-4
	{
		fahr = (celsius / (5.0/9.0)) + 32.0;
		printf("%3.0f %6.1f\n", celsius, fahr);
		// Could also make this -= and keep increment at 5
		celsius += increment;
	}
}

void for_loop_solution()
{
	float fahr, celsius;

	int start, end, increment;

	start 		= 300;
	end			= 0;
	increment   = -5;

	for(celsius = start; celsius >= end; celsius += increment)
	{
		fahr = (celsius / (5.0/9.0)) + 32.0;
		printf("%3.0f %6.1f\n", celsius, fahr);
	}
}
