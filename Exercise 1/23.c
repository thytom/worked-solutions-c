#include <stdio.h>
#define MAXLINE 1000

/* Why not make the comments it has to remove part of itself?
   ;) */

int main()
{
	char c;
	char last_char;
	/* only have to deal with ansi c comments */
	short in_comment = 0;

	while((c = getchar()) != EOF)
	{
		if(in_comment == 1)
		{
			if(c == '*' && (c = getchar()) == '/')
				in_comment = 0;
		}
		else if(in_comment == 0)
		{
			if((last_char = c) == '/' && (c = getchar()) == '*')
				in_comment = 1;
			else
				putchar(last_char);
		}
	}

	return 0;
}
