#include <stdio.h>
#define MAXLINE 1000

int get_line(char line[], int lim);
void reverse(char line[], int length);
void copy(char to[], char from[]);

int main()
{
	int line_length;
	char line[MAXLINE];

	while((line_length = get_line(line, MAXLINE)) > 0)
	{
		reverse(line, line_length);
	}
	return 0;
}

int get_line(char line[], int lim)
{
	int i, c;
	for(i = 0; i < lim -1  && (c = getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;
	if(c == '\n'){
		line[i] = c;
		++i;
	}
	line[i]   = '\0';
	return i;
}

void reverse(char line[], int length)
{
	for(int i = length - 2; i >= 0; i--)
	{
		putchar(line[i]);
	}
	putchar('\n');
}

void copy(char to[], char from[])
{
	int i;

	i = 0;
	while((to[i] = from[i]) != '\0')
		i++;
}
