#include <stdio.h>
#define MAXCOLUMN 80
#define MAXLINE 1000

/* Fold long lines over multiple columns */
int get_line(char line[], int lim);
int behead(char s[], int remove, int length);

int main()
{
	char s[MAXLINE];
	int length;
	int offset;

	/* take line */
	/* 	if it's short enough */
	/* 		do nothing */
	/* 	while it's too long */
	/* 		go to the column at the end + */
	/* 			while you move back to find a space */
	/* 				if you find a space */
	/* 					make it a newline */
	/* 					print up to the newline */
	/* 					set offset to where you stopped */
	/* 					break out */
	/* 				if we reach the end */

	while((length = get_line(s, MAXLINE)) > 0)
	{
		if(length < MAXCOLUMN)
			printf("%s", s);
		else
		{
			while(length > MAXCOLUMN)
			{
				int i = MAXCOLUMN;
				while(i > 0)
				{
					if(s[i] == ' ' || s[i] == '\t')
					{
						for(int j = 0; j < i; j++)
							putchar(s[j]);
						putchar('\n');
						length = behead(s, i + 1, length);
						break;
					}
					i--;
				}
				if(i == 0)
				{
					for(int j = 0; j < MAXCOLUMN; j++)
						putchar(s[j]);
					putchar('\n');
					length = behead(s, MAXCOLUMN, length);
				}
			}
			for(int i = 0; i < length; i++)
				putchar(s[i]);
		}
	}
	return 0;
}

int behead(char s[], int remove, int length)
{
	int i;
	int new_length = length - remove;
	for(i = 0; i < length && s[i + remove] != '\n'; i++)
		s[i] = s[i + remove];
	s[i] = '\n';
	s[new_length] = '\0';
	return new_length;
}

int get_line(char line[], int lim)
{
	int i, c;
	for(i = 0; i < lim -1  && (c = getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;
	if(c == '\n'){
		line[i] = c;
		++i;
	}
	line[i]   = '\0';
	return i;
}
