#include <stdio.h>
/*
NOTE:
	For these programs, they read from STDIN. They will appear not to
	work if you just run them.

	To test this program, you need to pipe it some input:

	echo "Test input" | ./program
   */

int main()
{
	char c;

	while((c = getchar()) != EOF)
	{
		if(c == '\t')
			printf("\\t");
		else if(c == '\b')
			printf("\\b");
		else if(c == '\\')
			printf("\\\\");

		putchar(c);
	}
}

/* You could also do the while statement like this:
	while((c = getchar()) != EOF)
	{
		switch(c)
		{
			case '\t' : printf("\\t"); continue;
			case '\b' : printf("\\b"); continue;
			case '\\' : printf("\\\\"); continue;

			default : break;
		}

		putchar(c);
	}
   */
