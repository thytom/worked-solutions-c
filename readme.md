The C Programming Language - Worked Solutions
=============================================

These are the worked solutions to the exercises in the book "The C Programming
Language, 2nd Edition" by Brian Kernighan and Dennis Ritchie.

Solutions are done with all the tools provided up to that point in the book. I
have included optional solutions using later tools, as an example of what a
better solution would look like.

# TODO:
Look at the challenge part of 1-13?
Improve 1-24. Works, but needs to have support for:
	- Quotes, both single and double,
	- Ignoring escape sequences
	- Comments
